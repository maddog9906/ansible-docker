# Ansible Docker

## Getting Started

To start ansible,

`docker-compose run anisble ash`

To scale to 3 nodes,
`docker-compose scale node=3 `

Once in the ansible console, to test do the following
`ansible all -m ping`

## Other Info
If `systemd` image is desired, there is an additional setup stage need. 
This is to prepare the `cgroup`.

```
docker run --rm --privileged -v /:/host alvin2439/ubuntu-systemd-sshd:latest setup
```

## References
http://mauricioklein.com/ansible/docker/2018/02/23/ansible-docker/
https://medium.com/@dcarrascal75/a-simple-ansible-playground-using-docker-eeb458cbba32
https://www.ansible.com/blog/how-i-switched-from-docker-compose-to-pure-ansible
https://nickjanetakis.com/blog/docker-and-ansible-solve-2-different-problems-and-they-can-be-used-together
https://www.ansible.com/blog/six-ways-ansible-makes-docker-compose-better
https://www.codementor.io/mamytianarakotomalala/how-to-deploy-docker-container-with-ansible-on-debian-8-mavm48kw0
https://github.com/solita/docker-systemd